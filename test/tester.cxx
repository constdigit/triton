#include <iostream>

#include "tester.hxx"

tester::tester(std::string filename, std::string mode)
  : _video_capture(filename)
  , _mode(mode)
{
  if (!_video_capture.isOpened()) {
    throw std::runtime_error("Unable to open: " + filename);
  }
  auto capture_width =
    static_cast<int>(_video_capture.get(cv::CAP_PROP_FRAME_WIDTH));
  auto capture_height =
    static_cast<int>(_video_capture.get(cv::CAP_PROP_FRAME_HEIGHT));

  if (_mode.find('w') != std::string::npos) {
    _video_writer.open("demo.avi",
                       _video_capture.get(cv::CAP_PROP_FOURCC),
                       _video_capture.get(cv::CAP_PROP_FPS),
                       cv::Size(capture_width, capture_height));
    if (!_video_writer.isOpened()) {
      throw std::runtime_error("Unable to open: " + filename);
    }
  }

  cv::Point roi_tl(capture_width * 0.05, capture_height * 0.45);
  cv::Point roi_br(capture_width * 0.95, capture_height * 0.9);
  auto roi_rect = cv::Rect(roi_tl, roi_br);
  _tracker.roi() = roi_rect;
  _yolo_detector.roi() = roi_rect;
  _motion_detector.roi() = roi_rect;

  _roadmap_builder =
    triton::dynamic_roadmap_builder(cv::Size(capture_width, capture_height));

  cv::namedWindow(_window_name.data());
}

tester::~tester()
{
  cv::destroyAllWindows();
}

void
tester::operator()()
{
  while (true) {
    // get next frame
    _video_capture >> _source_frame;
    _canvas = _source_frame.clone();
    cv::cvtColor(_source_frame, _gray_frame, cv::COLOR_BGR2GRAY);

    // work pipeline
    auto begin = std::chrono::system_clock::now();
    _detection();
    auto end = std::chrono::system_clock::now();
    std::cerr << "detection\t"
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                   .count()
              << '\n';
    begin = std::chrono::system_clock::now();
    _tracking();
    end = std::chrono::system_clock::now();
    std::cerr << "tracking\t"
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                   .count()
              << '\n';

    auto src_roi = _source_frame(_tracker.roi()).clone();
    cv::Mat mask = cv::Mat::zeros(_source_frame.size(), _source_frame.type());
    auto mask_roi = mask(_tracker.roi());
    src_roi.copyTo(mask_roi);
    cv::addWeighted(mask, 0.4, _canvas, 0.6, 0.0, _canvas);

    begin = std::chrono::system_clock::now();
    _build_roadmap();
    end = std::chrono::system_clock::now();
    std::cerr << "build roadmap\t"
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                   .count()
              << '\n';

    begin = std::chrono::system_clock::now();
    _density_estimation();
    end = std::chrono::system_clock::now();
    std::cerr << "density estimation\t"
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                   .count()
              << '\n';

    _draw_boxes();
    // show the current frame
    cv::putText(_canvas,
                "frame: " + std::to_string(_frame_id),
                cv::Point(10, 20),
                cv::FONT_HERSHEY_SIMPLEX,
                0.5,
                cv::Scalar::all(255));
    // passed vehicles count
    _draw_vehicle_count();
    cv::imshow(_window_name.data(), _canvas);
    _write_video();

    // get the input from the keyboard
    auto keyboard = cv::waitKey(_delay);
    if (keyboard == 'q' || keyboard == 27) {
      break;
    }
    if (keyboard == 'p') {
      _delay = _delay == 40 ? 0 : 40;
    }

    _frame_id++;
  }
}

void
tester::_detection()
{
  auto motion = _mode.find('m') != std::string::npos;
  auto yolo = _mode.find('y') != std::string::npos;
  if (!motion && !yolo) {
    if (_detected_areas.empty()) {
      _detected_areas.emplace_back(
        std::make_pair(cv::Rect(cv::Point(0, 0), _source_frame.size()),
                       triton::basic_detector::vehicle_type::unknown));
    }
    return;
  }

  if (motion) {
    _detected_areas = _motion_detector(_gray_frame);
  } else {
    _detected_areas = _yolo_detector(_source_frame);
  }
}

void
tester::_tracking()
{
  if (_mode.find('t') == std::string::npos) {
    return;
  }

  auto status = _tracker(_gray_frame, _detected_areas);

  for (auto i = 0; i < status.size(); i++) {
    if (!status[i]) {
      _vehicle_count++;
      switch (_detected_areas[i].second) {
        case triton::basic_detector::vehicle_type::car:
          _car_count++;
          break;
        case triton::basic_detector::vehicle_type::bus:
          _bus_count++;
          break;
        case triton::basic_detector::vehicle_type::truck:
          _truck_count++;
          break;
        case triton::basic_detector::vehicle_type::bike:
          _bike_count++;
          break;
        default:
          _unknown_count++;
          break;
      }
    }
  }

  //  auto all_features_2f = _tracker.all_features();
  //  std::vector<cv::Point> all_features_2i;
  //  cv::Mat(all_features_2f)
  //    .convertTo(all_features_2i, cv::Mat(all_features_2i).type());
  //  for (auto& pt : all_features_2i) {
  //    cv::circle(_canvas, pt, 4, cv::Scalar::all(255), cv::FILLED);
  //  }
  //  cv::imwrite("features.png", _canvas);

  //  auto tracks = _tracker.all_tracks();
  //  for (auto& track_2f : tracks) {
  //    std::vector<cv::Point> track_2i;
  //    cv::Mat(track_2f).convertTo(track_2i, cv::Mat(track_2i).type());
  //    cv::polylines(_canvas, track_2i, false, cv::Scalar::all(255), 2);
  //  }
  //
  //  cv::imwrite("tracks.png", _canvas);
}

void
tester::_build_roadmap()
{
  for (auto& area : _detected_areas) {
    _roadmap_builder + area.first;
  }
}

void
tester::_density_estimation()
{
  auto density = _density_estimator(_gray_frame, _roadmap_builder.mask());
  cv::putText(_canvas,
              "density: " + std::to_string(density),
              cv::Point(10, 160),
              cv::FONT_HERSHEY_SIMPLEX,
              0.5,
              cv::Scalar::all(255));

  auto vehicles_area = 0;
  for (auto& area : _detected_areas) {
    vehicles_area += area.first.area();
  }

  auto roi_area = cv::countNonZero(_roadmap_builder.mask());
  density = static_cast<double>(vehicles_area) / roi_area;
  cv::putText(_canvas,
              "density: " + std::to_string(density),
              cv::Point(10, 180),
              cv::FONT_HERSHEY_SIMPLEX,
              0.5,
              cv::Scalar::all(255));
}

void
tester::_draw_boxes()
{
  for (auto& object : _detected_areas) {
    auto object_color =
      _types_colors[static_cast<int>(object.second) % _types_colors.size()];
    auto object_class_name =
      triton::basic_detector::vehicle_type_string(object.second);
    auto baseline = 0;
    auto label_size = cv::getTextSize(
      object_class_name, cv::FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseline);

    cv::rectangle(_canvas, object.first, object_color);
    cv::rectangle(
      _canvas,
      cv::Rect(object.first.tl(),
               cv::Size(label_size.width, label_size.height + baseline)),
      object_color,
      cv::FILLED);
    cv::putText(_canvas,
                object_class_name,
                object.first.tl() + cv::Point(0, label_size.height),
                cv::FONT_HERSHEY_SIMPLEX,
                0.5,
                cv::Scalar::all(255));
  }
}

void
tester::_draw_vehicle_count() const
{
  cv::putText(_canvas,
              "total: " + std::to_string(_vehicle_count),
              cv::Point(10, 40),
              cv::FONT_HERSHEY_SIMPLEX,
              0.5,
              cv::Scalar::all(255));
  cv::putText(_canvas,
              "car: " + std::to_string(_car_count),
              cv::Point(10, 60),
              cv::FONT_HERSHEY_SIMPLEX,
              0.5,
              cv::Scalar::all(255));
  cv::putText(_canvas,
              "bus: " + std::to_string(_bus_count),
              cv::Point(10, 80),
              cv::FONT_HERSHEY_SIMPLEX,
              0.5,
              cv::Scalar::all(255));
  cv::putText(_canvas,
              "truck: " + std::to_string(_truck_count),
              cv::Point(10, 100),
              cv::FONT_HERSHEY_SIMPLEX,
              0.5,
              cv::Scalar::all(255));
  cv::putText(_canvas,
              "bike: " + std::to_string(_bike_count),
              cv::Point(10, 120),
              cv::FONT_HERSHEY_SIMPLEX,
              0.5,
              cv::Scalar::all(255));
  cv::putText(_canvas,
              "unknown: " + std::to_string(_unknown_count),
              cv::Point(10, 140),
              cv::FONT_HERSHEY_SIMPLEX,
              0.5,
              cv::Scalar::all(255));
}

void
tester::_write_video()
{
  if (_mode.find('w') != std::string::npos) {
    _video_writer << _canvas;
  }
}