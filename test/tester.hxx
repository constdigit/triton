#pragma once

#include <future>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>

#include "density_estimator.hxx"
#include "dynamic_roadmap_builder.hxx"
#include "motion_detector.hxx"
#include "tracker.hxx"
#include "yolo_detector.hxx"

class tester
{
public:
  tester(std::string filename, std::string mode);
  ~tester();
  void operator()();

private:
  static constexpr std::string_view _window_name = "Test";
  cv::VideoCapture _video_capture;
  cv::VideoWriter _video_writer;
  cv::Mat _source_frame;
  cv::Mat _canvas;
  cv::Mat _gray_frame;
  int _delay{ 40 };
  int _frame_id{ 0 };
  std::string _mode;

  triton::motion_detector _motion_detector;
  std::vector<triton::basic_detector::detection_box_t> _detected_areas;
  triton::yolo_detector _yolo_detector{ "../../yolov2.cfg",
                                        "../../yolov2.weights",
                                        "../../coco.names",
                                        0.4f };
  std::array<cv::Scalar, 8> _types_colors{ cv::Scalar{ 0x7e, 0x9f, 0x18 },
                                           cv::Scalar{ 0xb2, 0xa5, 0xaf },
                                           cv::Scalar{ 0xd0, 0x17, 0xf8 },
                                           cv::Scalar{ 0xe5, 0x7d, 0xd6 },
                                           cv::Scalar{ 0x12, 0x2d, 0xb8 } };

  triton::tracker _tracker;
  cv::Scalar _tracks_color{ 215, 200, 100 };
  uint64_t _vehicle_count{ 0 };
  uint64_t _car_count{ 0 };
  uint64_t _bus_count{ 0 };
  uint64_t _truck_count{ 0 };
  uint64_t _bike_count{ 0 };
  uint64_t _unknown_count{ 0 };

  triton::dynamic_roadmap_builder _roadmap_builder;
  triton::density_estimator _density_estimator;

  void _detection();
  void _tracking();
  void _build_roadmap();
  void _density_estimation();
  void _draw_boxes();
  void _draw_vehicle_count() const;
  void _write_video();
};
