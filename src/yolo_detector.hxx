#pragma once

#include <opencv2/dnn.hpp>

#include "basic_detector.hxx"

namespace triton {
/**
 * \brief Vehicles detection based on YOLO CNN.
 * \see triton::basic_detector
 * This class uses YOLOv2 neural network for
 * vehicles detection and classification.
 */
class yolo_detector : public basic_detector
{
public:
  /// Default \c cfg file that supposed to be in cwd.
  static constexpr std::string_view default_cfg = "yolo.cfg";
  /// Default model file that supposed to be in cwd.
  static constexpr std::string_view default_model = "yolo.weights";
  /// Default classes names file that supposed to be in cwd.
  static constexpr std::string_view default_names = "yolo.names";
  /// Default confidence threshold value for filtration.
  static constexpr float default_confidence_threshold = 0.25f;

  /**
   * \brief Default constructor.
   * Loads YOLOv2 neural net with given configuration.
   * \param cfg Path to configuration file.
   * \param model Path to trained model's weights file.
   * \param names Path to file that contains classes names.
   * \param confidence_threshold Custom confidence threshold.
   */
  explicit yolo_detector(
    std::string_view cfg = default_cfg,
    std::string_view model = default_model,
    std::string_view names = default_names,
    float confidence_threshold = default_confidence_threshold);

  /**
   * \brief Vehicles detection and classification.
   * \copydoc triton::basic_detector::operator()(const cv::Mat& capture)
   */
  std::vector<detection_box_t> operator()(const cv::Mat& capture) override;

private:
  static constexpr std::string_view _input_layer = "data";
  static constexpr std::string_view _output_layer = "detection_out";
  static constexpr int _probability_index = 5;
  static constexpr float _minimal_width = 50;
  static constexpr float _minimal_height = 50;

  float _confidence_threshold{ 0.f };
  cv::Size _blob_size;
  cv::dnn::Net _yolo_network;
  std::vector<std::string> _classes_names;

  std::string _class_name(size_t class_id) const;
  basic_detector::vehicle_type _vehicle_type(std::string_view class_name) const;
  basic_detector::vehicle_type _vehicle_type(size_t class_id) const;
};
}