#pragma once

#include <opencv2/core.hpp>

namespace triton {
/**
 * Auxiliary class for traffic density estimation.
 * Can be used for dynamically building road map.
 */
class dynamic_roadmap_builder
{
public:
  /// Use it only if capture size not known in advance.
  dynamic_roadmap_builder() = default;

  /**
   * Initialize empty road map with given size.
   * \param capture_size Width and height of capture.
   */
  explicit dynamic_roadmap_builder(cv::Size capture_size);

  /**
   * Extends road map by given area.
   * \param area New piece of road map.
   */
  void operator+(const cv::Rect& area);

  /// Getter for binary road map.
  const cv::Mat& mask();

private:
  cv::Mat _roadmap_mask;
};
}
