#pragma once

namespace triton {
/**
 * \brief Vehicle detector interface.
 * Provides basic types and methods for vehicle detection
 * and classification.
 */
class basic_detector
{
public:
  /// How many different classes can be recognized.
  static constexpr size_t vehicle_types_amount = 5;

  /// List of supported vehicle types.
  enum class vehicle_type
  {
    car,
    bus,
    truck,
    bike,
    unknown
  };

  /// Helpful alias that holds vehicle bounding rect and its type.
  using detection_box_t = std::pair<cv::Rect, vehicle_type>;

  /**
   * \brief Process detection.
   * Obtains information about vehicles on given image.
   * Predicts coordinates, sizes and types.
   * \param capture RGB, 8 bit  source image.
   * \return Vector in which each element is a detected vehicle.
   */
  virtual std::vector<detection_box_t> operator()(const cv::Mat& capture) = 0;

  /**
   * \brief Vehicle type conversion.
   * Converts elements of \c vehicle_type enumeration
   * into \c std::string .
   * \param type Item of \c vehicle_type  enum that should be converted.
   * \return String representation of given type.
   */
  static std::string vehicle_type_string(vehicle_type type)
  {
    switch (type) {
      case vehicle_type::car:
        return "car";
      case vehicle_type::bus:
        return "bus";
      case vehicle_type::truck:
        return "truck";
      case vehicle_type::bike:
        return "bike";
      case vehicle_type::unknown:
        return "unknown";
    }
  }

  /**
   * ROI access method.
   * \return Reference to currently used ROI.
   */
  cv::Rect& roi() { return _roi; }

protected:
  /// Region of interest.
  cv::Rect _roi;

  /**
   * Check for vehicles location.
   * \param detected Detected vehicle's bounding rect.
   * \return True if it's outside of ROI.
   */
  bool _is_outside(const cv::Rect& detected) const
  {
    auto intersection = detected & _roi;
    return intersection.area() < detected.area();
  }
};
}
