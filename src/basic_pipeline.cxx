#include "basic_pipeline.hxx"

using namespace triton;

basic_pipeline::basic_pipeline(cv::Size capture_size, cv::Rect roi)
{
  _detector = std::make_unique<motion_detector>();
  _detector->roi() = roi;
  _tracker.roi() = roi;
  _roadmap_builder = triton::dynamic_roadmap_builder(capture_size);
  std::fill(_vehicles_count.begin(), _vehicles_count.end(), 0);
}

basic_pipeline::basic_pipeline(cv::Size capture_size)
  : basic_pipeline(capture_size, cv::Rect(cv::Point(0, 0), capture_size))
{}

const std::vector<basic_detector::detection_box_t>&
basic_pipeline::operator()(const cv::Mat& capture)
{
  _source_frame = capture;
  cv::cvtColor(capture, _gray_frame, cv::COLOR_BGR2GRAY);

  _detection();
  _tracking();
  _build_roadmap();
  _density_estimation();

  return _detected_areas;
}

void
basic_pipeline::enable_motion_detector(int history_length,
                                       double variance_threshold)
{
  auto roi = _detector->roi();
  _detector =
    std::make_unique<motion_detector>(history_length, variance_threshold);
  _detector->roi() = roi;
  _detection_frame = &_gray_frame;
}

void
basic_pipeline::enable_yolo_detector(std::string_view cfg,
                                     std::string_view model,
                                     std::string_view names,
                                     float confidence_threshold)
{
  auto roi = _detector->roi();
  _detector =
    std::make_unique<yolo_detector>(cfg, model, names, confidence_threshold);
  _detector->roi() = roi;
  _detection_frame = &_source_frame;
}

void
basic_pipeline::enable_alternative_density_estimation()
{
  _is_alternative_density_estimation_enabled = true;
}

void
basic_pipeline::enable_simple_density_estimation()
{
  _is_alternative_density_estimation_enabled = false;
}

std::array<uint64_t, basic_detector::vehicle_types_amount>
basic_pipeline::accumulated_vehicles_count()
{
  return _vehicles_count;
}

std::vector<std::vector<cv::Point2f>>
basic_pipeline::actual_tracks() const
{
  return _tracker.all_tracks();
}

double
basic_pipeline::current_density() const
{
  return _traffic_density;
}

void
basic_pipeline::_detection()
{
  _detected_areas = (*_detector)(*_detection_frame);
}

void
basic_pipeline::_tracking()
{
  auto status = _tracker(_gray_frame, _detected_areas);

  for (auto i = 0; i < status.size(); i++) {
    if (!status[i]) {
      _vehicles_count[static_cast<int>(_detected_areas[i].second)]++;
    }
  }
}

void
basic_pipeline::_build_roadmap()
{
  for (auto& area : _detected_areas) {
    _roadmap_builder + area.first;
  }
}

void
basic_pipeline::_density_estimation()
{
  if (_is_alternative_density_estimation_enabled) {
    _traffic_density = _density_estimator(_gray_frame, _roadmap_builder.mask());
  } else {
    auto vehicles_area = 0;
    for (auto& area : _detected_areas) {
      vehicles_area += area.first.area();
    }

    auto roi_area = cv::countNonZero(_roadmap_builder.mask());

    _traffic_density = static_cast<double>(vehicles_area) / roi_area;
  }
}
