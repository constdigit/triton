#include <memory>

#include "density_estimator.hxx"
#include "dynamic_roadmap_builder.hxx"
#include "motion_detector.hxx"
#include "tracker.hxx"
#include "yolo_detector.hxx"

namespace triton {
/**
 * Class-mediator that connects all modules together in sequential order.
 * Also provides basic public API.
 */
class basic_pipeline
{
public:
  /**
   * Make new \c basic_pipeline that will be works on captures with
   * specified size and ROI.
   * By default uses \c motion_detector and alternative density estimation.
   * \param capture_size Expected source images size.
   * \param roi Region of interest for all algorithms.
   */
  basic_pipeline(cv::Size capture_size, cv::Rect roi);

  /// Overloaded constructor that uses whole image as ROI.
  basic_pipeline(cv::Size capture_size);

  virtual ~basic_pipeline() = default;

  /**
   * Sequentially runs detection, tracking and density estimation.
   * \param capture Source RGB 8-bit image.
   * \return Vector of currently detected and tracked vehicles.
   */
  virtual const std::vector<basic_detector::detection_box_t>& operator()(
    const cv::Mat& capture);

  /** Switch detector instance to \c motion_detector.
   * \see triton::motion_detector
   */
  virtual void enable_motion_detector(
    int history_length = motion_detector::default_history_length,
    double variance_threshold = motion_detector::default_variance_threshold);

  /** Switch detector instance to \c motion_detector.
   * \see triton::yolo_detector
   */
  virtual void enable_yolo_detector(
    std::string_view cfg = yolo_detector::default_cfg,
    std::string_view model = yolo_detector::default_model,
    std::string_view names = yolo_detector::default_names,
    float confidence_threshold = yolo_detector::default_confidence_threshold);

  /// Deduce density using \c density_estimator class.
  virtual void enable_alternative_density_estimation();

  /// Deduce density using detection boxes areas.
  virtual void enable_simple_density_estimation();

  /**
   * Usage:
   * \code
    auto current_vehicles_count = pipeline.accumulated_vehicles_count();
    for (auto type_index = 0;
      type_index <= triton::basic_detector::vehicle_types_amount;
      type_index++) {
      switch (static_cast<triton::basic_detector::vehicle_type>(type_index)) {
        case triton::basic_detector::vehicle_type::car :
            std::cout << "Cars count: " << current_vehicles_count[type_index];
            break;
        case triton::basic_detector::vehicle_type::bus :
            std::cout << "Buses count: " << current_vehicles_count[type_index];
            break;
        case triton::basic_detector::vehicle_type::truck :
            std::cout << "Trucks count: " << current_vehicles_count[type_index];
            break;
        case triton::basic_detector::vehicle_type::bike :
            std::cout << "Bikes count: " << current_vehicles_count[type_index];
            break;
        case triton::basic_detector::vehicle_type::unknown :
            std::cout << "Unknowns count: " <<
            current_vehicles_count[type_index];
            break;
      }
    }
   * \endcode
   * \return Currently counted amount of vehicles of each type.
   */
  virtual std::array<uint64_t, basic_detector::vehicle_types_amount>
  accumulated_vehicles_count();

  /**
   * \see triton::tracker::all_tracks()
   * \return Actual tracks for all vehicles.
   */
  virtual std::vector<std::vector<cv::Point2f>> actual_tracks() const;

  /// Returns approximate traffic density for current frame.
  virtual double current_density() const;

protected:
  cv::Mat _source_frame;
  cv::Mat _gray_frame;
  cv::Mat* _detection_frame;
  std::vector<basic_detector::detection_box_t> _detected_areas;
  std::array<uint64_t, basic_detector::vehicle_types_amount> _vehicles_count;
  double _traffic_density{ 0.0 };
  bool _is_alternative_density_estimation_enabled{ true };
  std::unique_ptr<triton::basic_detector> _detector;
  tracker _tracker;
  dynamic_roadmap_builder _roadmap_builder;
  density_estimator _density_estimator;

  virtual void _detection();
  virtual void _tracking();
  virtual void _build_roadmap();
  virtual void _density_estimation();
};
}
