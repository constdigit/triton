#include <opencv2/imgproc.hpp>

#include "dynamic_roadmap_builder.hxx"

triton::dynamic_roadmap_builder::dynamic_roadmap_builder(cv::Size capture_size)
  : _roadmap_mask(cv::Mat::zeros(capture_size, CV_8UC1))
{}

void
triton::dynamic_roadmap_builder::operator+(const cv::Rect& area)
{
  cv::rectangle(_roadmap_mask, area, cv::Scalar::all(255), cv::FILLED);
}

const cv::Mat&
triton::dynamic_roadmap_builder::mask()
{
  return _roadmap_mask;
}
