#pragma once

#include <opencv2/core.hpp>

namespace triton {
/**
 * This class is used for estimation of road traffic density.
 */
class density_estimator
{
private:
  double _clahe_clip_limit{ 1.0 };
  int _canny_threshold1{ 100 };
  int _canny_threshold2{ 120 };
  int _blur_window_size{ 49 };
  double _binary_threshold{ 230.0 };
  double _binary_maxval{ 255.0 };

public:
  /**
   * Deduces traffic density on given capture using
   * binary mask of road map.
   * \param capture Treated source image.
   * \param mask Binary road map (the closer it to reality the better results).
   * \return Approximate traffic density.
   */
  double operator()(const cv::Mat& capture, const cv::Mat& mask);
};
}
