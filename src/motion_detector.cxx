#include <iostream>

#include <opencv2/highgui.hpp>

#include "motion_detector.hxx"

using namespace triton;

motion_detector::motion_detector(int history_length, double variance_threshold)
  : _background_subtractor{
    cv::createBackgroundSubtractorMOG2(history_length, variance_threshold)
  }
{}

std::vector<basic_detector::detection_box_t>
motion_detector::operator()(const cv::Mat& capture)
{
  _areas.clear();
  _scale_ratio_x = static_cast<double>(capture.cols) / _foreground_mask_width;
  _scale_ratio_y = static_cast<double>(capture.rows) / _foreground_mask_height;
  cv::resize(capture,
             _capture,
             cv::Size(_foreground_mask_width, _foreground_mask_height),
             0.0,
             0.0,
             cv::INTER_AREA);
  cv::normalize(_capture, _capture, 0, 255, cv::NORM_MINMAX);

  _subtraction();
  _filter_foreground_mask();
  _find_connected_components();
  _filter_areas();
  _scale_areas();
  _areas.erase(
    std::remove_if(_areas.begin(),
                   _areas.end(),
                   [this](auto& area) { return _is_outside(area.first); }),
    _areas.end());

  return _areas;
}

void
motion_detector::_subtraction()
{
  _background_subtractor->apply(_capture, _foreground_mask);
  cv::threshold(_foreground_mask, _foreground_mask, 254, 1, cv::THRESH_BINARY);
}

void
motion_detector::_filter_foreground_mask()
{
  cv::morphologyEx(
    _foreground_mask, _foreground_mask, cv::MORPH_CLOSE, _kernel_7x7);
  cv::morphologyEx(
    _foreground_mask, _foreground_mask, cv::MORPH_OPEN, _kernel_3x3);
}

void
motion_detector::_find_connected_components()
{
  cv::Mat labels, stats, centroids;
  auto labels_count = cv::connectedComponentsWithStats(
    _foreground_mask, labels, stats, centroids);

  for (auto i = 1; i < labels_count; i++) {
    auto x = stats.at<int>(i, cv::CC_STAT_LEFT);
    auto y = stats.at<int>(i, cv::CC_STAT_TOP);
    auto w = stats.at<int>(i, cv::CC_STAT_WIDTH);
    auto h = stats.at<int>(i, cv::CC_STAT_HEIGHT);

    _areas.emplace_back(std::make_pair(cv::Rect(x, y, w, h),
                                       basic_detector::vehicle_type::unknown));
  }
}

void
motion_detector::_filter_areas()
{
  if (_areas.empty()) {
    return;
  }
  _areas.erase(std::remove_if(_areas.begin(),
                              _areas.end(),
                              [this](auto& area) {
                                return area.first.width < _area_min_width ||
                                       area.first.height < _area_min_height;
                              }),
               _areas.end());
  _remove_nested();
}

void
motion_detector::_remove_nested()
{
  if (_areas.empty()) {
    return;
  }
  for (auto test_area_it = _areas.begin(); test_area_it < _areas.end() - 1;
       test_area_it++) {
    std::for_each(
      test_area_it + 1, _areas.end(), [test_area = *test_area_it](auto& area) {
        auto intersection = (area.first & test_area.first);
        if (intersection == area.first) {
          area.first = cv::Rect();
        }
      });
  }
  _areas.erase(
    std::remove_if(_areas.begin(),
                   _areas.end(),
                   [](auto& area) { return area.first.area() == 0; }),
    _areas.end());
}

void
motion_detector::_scale_areas()
{
  std::for_each(_areas.begin(), _areas.end(), [this](auto& area) {
    area.first.x *= _scale_ratio_x;
    area.first.width *= _scale_ratio_x;
    area.first.y *= _scale_ratio_y;
    area.first.height *= _scale_ratio_y;
  });
}

void
motion_detector::apply_mask(cv::Mat& capture)
{
  cv::Mat full_size_foreground_mask;
  cv::resize(_foreground_mask, full_size_foreground_mask, capture.size());
  cv::multiply(capture, full_size_foreground_mask, capture);
}
