#pragma once

#include <chrono>

#include <opencv2/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/videoio.hpp>

#include "basic_detector.hxx"

namespace triton {
/**
 * \brief Vehicles tracking.
 * This class provides access to methods for tracking vehicles.
 * Works with conjunction with detector.
 */
class tracker
{
public:
  /**
   * Updates existing tracks.
   * \param capture Next frame.
   * \return Statuses of each track (false for passed).
   */
  std::vector<bool> operator()(const cv::Mat& capture);

  /**
   * Updates existing tracks and initializes new.
   * \param capture Next frame.
   * \param detected_areas Result of detector on given image.
   * \return Statuses of each track (false for passed).
   */
  std::vector<bool> operator()(
    const cv::Mat& capture,
    std::vector<basic_detector::detection_box_t>& detected_areas);

  /// Getter for all currently tracked points.
  std::vector<cv::Point2f> all_features() const;

  /**
   * \brief Getter for laid vehicles trajectories.
   * Track - vector of vehicle's centroids on every frame.
   * Retrieving result should look like:
   * \c tracks[vehicle_index][centroid_index]
   * \return Actual tracks for all vehicles.
   */
  std::vector<std::vector<cv::Point2f>> all_tracks() const;

  /**
   * ROI access method.
   * \return Reference to currently used ROI.
   */
  cv::Rect& roi();

private:
  static constexpr int _corners_amount_limit = 100;
  static constexpr double _corners_quality = 0.01;
  static constexpr double _corners_distance = 10;
  class track;
  cv::TermCriteria _criteria{ cv::TermCriteria::COUNT | cv::TermCriteria::EPS,
                              20,
                              0.03 };
  cv::Size _corners_refining_window{ 10, 10 };
  cv::Size _optical_flow_window{ 31, 31 };
  cv::Mat _previous_capture;
  cv::Mat _capture;
  cv::Rect _roi;
  std::vector<track> _tracks{};

  void _amend_detection(
    std::vector<basic_detector::detection_box_t>& detected_areas) const;
  void _apply_optical_flow();
  std::vector<cv::Point2f> _initialize_features(const cv::Rect& area);
  void _remove_irrelevant_tracks();
  std::vector<bool> _tracks_status() const;
  void _update_tracks_by_new_vehicles(
    std::vector<basic_detector::detection_box_t>& detected_areas);
};

/**
 * Auxiliary internal class.
 * Represents separate track.
 */
class tracker::track
{
public:
  /**
   * Constructs new track with given feature points and type.
   * \param features Points to track.
   * \param type Tracked vehicle type.
   */
  track(std::vector<cv::Point2f> features, basic_detector::vehicle_type type);

  /**
   * Updates track by features from next frame.
   * \param features Tracked points coordinates on next frame.
   */
  void append(std::vector<cv::Point2f> features);

  /// Returns approximated bounding rect of tracked vehicle.
  cv::Rect bounding_rect() const;

  /// Returns feature points on current frame.
  const std::vector<cv::Point2f>& features() const;

  /// Returns laid trajectory.
  const std::vector<cv::Point2f>& history() const;

  /// True if track wasn't updated during a few frames.
  bool is_expired() const;

  /// True if there are no one point to track.
  bool is_invisible() const;

  /// True if there are too few points to track.
  bool is_laked() const;

  /// True if tracked vehicle is outside of ROI.
  bool is_outside(const cv::Rect& roi) const;

  /// True if vehicle doesn't moves during a few frames.
  bool is_static() const;

  /// Returns last point in trajectory.
  const cv::Point2f& last_centroid() const;

  /**
   * Filters appended feature points.
   * \param status Appropriate statuses obtained from cv::calcOpticalFlowPyrLK.
   * \param errors Appropriate errors obtained from cv::calcOpticalFlowPyrLK.
   */
  void refine_features(std::vector<uint8_t> status, std::vector<float> errors);

  /**
   * Makes copy of this track but with different \c id.
   * \return Pair of current track and his legatee.
   */
  std::pair<track, track> split();

  /// Returns time stamp of last update.
  std::chrono::steady_clock::time_point stamp() const;

  /// Returns type of tracked vehicle.
  basic_detector::vehicle_type type() const;

private:
  static constexpr size_t _minimum_features{ 5 };
  static constexpr uint64_t _dispose_timeout{ 2 };
  static constexpr float _error_limit{ 20.0 };
  static constexpr float _variation_limit{ 100.0 };
  static constexpr float _distance_epsilon{ 1.0 };
  static constexpr size_t _still_limit{ 10 };
  static uint64_t _leading_id;
  std::vector<cv::Point2f> _features;
  std::vector<cv::Point2f> _history;
  uint64_t _id{};
  std::chrono::steady_clock::time_point _stamp;
  basic_detector::vehicle_type _type;

  track() = default;
  cv::Point2f _centroid() const;
};
}