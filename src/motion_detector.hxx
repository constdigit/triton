#pragma once

#include <cmath>
#include <vector>

#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>

#include "basic_detector.hxx"

namespace triton {
/**
 * \brief Vehicles detection based on background subtraction.
 * \see triton::basic_detector
 * This class can detect moving objects using MOG2 algorithm.
 */
class motion_detector : public basic_detector
{
public:
  /// Default history depth for background model.
  static constexpr int default_history_length = 500;
  /// Setup default sensitivity of subtraction.
  static constexpr double default_variance_threshold = 64;

  /**
   * \brief Default constructor.
   * You can setup MOG2 algorithm in this place.
   * \param history_length Depth for background model.
   * \param variance_threshold Sensitivity of subtraction.
   */
  motion_detector(int history_length = default_history_length,
                  double variance_threshold = default_variance_threshold);
  void apply_mask(cv::Mat& capture);

  /**
   * \brief Moving vehicles detection.
   * \copydoc triton::basic_detector::operator()(const cv::Mat& capture)
   * This implementation can't deduce vehicles types, so all of them is
   * \c unknown.
   */
  std::vector<detection_box_t> operator()(const cv::Mat& capture) override;

private:
  static constexpr int _foreground_mask_width = 256;
  static constexpr int _foreground_mask_height = 144;
  static constexpr int _area_min_width = 11;
  static constexpr int _area_min_height = 11;

  cv::Ptr<cv::BackgroundSubtractorMOG2> _background_subtractor;
  cv::Mat _capture;
  cv::Mat _foreground_mask;
  cv::Mat _kernel_3x3{ cv::getStructuringElement(cv::MORPH_RECT,
                                                 cv::Size(3, 3)) };
  cv::Mat _kernel_7x7{ cv::getStructuringElement(cv::MORPH_RECT,
                                                 cv::Size(7, 7)) };
  std::vector<detection_box_t> _areas;
  double _scale_ratio_x;
  double _scale_ratio_y;

  void _subtraction();
  void _filter_foreground_mask();
  void _find_connected_components();
  void _filter_areas();
  void _remove_nested();
  void _scale_areas();
};
}