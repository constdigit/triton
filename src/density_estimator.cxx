#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "density_estimator.hxx"

using namespace triton;

double
density_estimator::operator()(const cv::Mat& capture, const cv::Mat& mask)
{
  cv::Mat edges;
  cv::Canny(capture, edges, _canny_threshold1, _canny_threshold2);
  edges = ~edges;

  cv::Mat blur;
  cv::blur(edges, blur, cv::Size(_blur_window_size, _blur_window_size));

  cv::Mat binary;
  cv::threshold(
    blur, binary, _binary_threshold, _binary_maxval, cv::THRESH_BINARY);

  cv::bitwise_and(binary, mask, binary);

  auto background_pixels_count = static_cast<double>(cv::countNonZero(binary));
  if (background_pixels_count == 0.0) {
    return 0.0;
  }
  auto all_pixels_count = static_cast<double>(cv::countNonZero(mask));
  if (all_pixels_count == 0.0) {
    return 0.0;
  }

  return 1.0 - background_pixels_count / all_pixels_count;
}
