#include <algorithm>
#include <utility>

#include "tracker.hxx"

using namespace triton;

std::vector<bool>
tracker::operator()(const cv::Mat& capture)
{
  _previous_capture = _capture.clone();
  _capture = capture.clone();

  _apply_optical_flow();

  auto status = _tracks_status();

  _remove_irrelevant_tracks();

  return status;
}

std::vector<bool>
tracker::operator()(
  const cv::Mat& capture,
  std::vector<basic_detector::detection_box_t>& detected_areas)
{
  _previous_capture = _capture.clone();
  _capture = capture.clone();

  _apply_optical_flow();

  _update_tracks_by_new_vehicles(detected_areas);
  _amend_detection(detected_areas);

  auto status = _tracks_status();

  _remove_irrelevant_tracks();

  return status;
}

std::vector<cv::Point2f>
tracker::all_features() const
{
  std::vector<cv::Point2f> output_features;
  for (auto& t : _tracks) {
    auto& local_features = t.features();
    output_features.insert(
      output_features.end(), local_features.begin(), local_features.end());
  }
  return output_features;
}

std::vector<std::vector<cv::Point2f>>
tracker::all_tracks() const
{
  std::vector<std::vector<cv::Point2f>> tracks(_tracks.size());
  auto internal_tracks_iterator = _tracks.begin();
  std::generate(tracks.begin(), tracks.end(), [&internal_tracks_iterator]() {
    return (internal_tracks_iterator++)->history();
  });
  return tracks;
}

cv::Rect&
tracker::roi()
{
  return _roi;
}

void
tracker::_amend_detection(
  std::vector<basic_detector::detection_box_t>& detected_areas) const
{
  for (auto& tracked : _tracks) {
    auto matched = std::find_if(
      detected_areas.begin(), detected_areas.end(), [&tracked](auto& detected) {
        auto intersection = detected.first & tracked.bounding_rect();
        return intersection.area() >= detected.first.area() / 2 ||
               intersection.area() >= tracked.bounding_rect().area() / 2;
      });
    if (matched == detected_areas.end()) {
      detected_areas.emplace_back(
        std::make_pair(tracked.bounding_rect(), tracked.type()));
    }
  }
}

void
tracker::_apply_optical_flow()
{
  auto previous_points = all_features();
  if (previous_points.empty()) {
    return;
  }
  std::vector<cv::Point2f> points;
  std::vector<uint8_t> status;
  std::vector<float> errors;

  cv::calcOpticalFlowPyrLK(_previous_capture,
                           _capture,
                           previous_points,
                           points,
                           status,
                           errors,
                           _optical_flow_window);

  auto points_begin = points.begin();
  auto status_begin = status.begin();
  auto errors_begin = errors.begin();
  for (auto& track : _tracks) {
    auto features_count = track.features().size();
    track.append(
      std::vector<cv::Point2f>(points_begin, points_begin + features_count));
    points_begin += features_count;

    track.refine_features(
      std::vector<uint8_t>(status_begin, status_begin + features_count),
      std::vector<float>(errors_begin, errors_begin + features_count));
    status_begin += features_count;
    errors_begin += features_count;
  }
}

std::vector<cv::Point2f>
tracker::_initialize_features(const cv::Rect& area)
{
  std::vector<cv::Point2f> features;
  auto roi = _capture(area);
  cv::goodFeaturesToTrack(
    roi, features, _corners_amount_limit, _corners_quality, _corners_distance);
  cv::cornerSubPix(
    roi, features, _corners_refining_window, cv::Size(-1, -1), _criteria);

  std::for_each(
    features.begin(), features.end(), [offset = area.tl()](auto& pt) {
      pt += static_cast<cv::Point2f>(offset);
    });
  return features;
}

void
tracker::_remove_irrelevant_tracks()
{
  _tracks.erase(std::remove_if(_tracks.begin(),
                               _tracks.end(),
                               [this](auto& track) {
                                 return track.is_laked() ||
                                        track.is_outside(_roi) ||
                                        track.is_static();
                               }),
                _tracks.end());
}

std::vector<bool>
tracker::_tracks_status() const
{
  std::vector<bool> status(_tracks.size());
  auto track_iterator = _tracks.begin();
  std::generate(status.begin(), status.end(), [&track_iterator, this]() {
    auto passed = track_iterator->is_outside(_roi);
    auto missed = track_iterator->is_expired();
    auto laked = track_iterator->is_laked();
    track_iterator++;
    return !(passed || missed || laked);
  });
  return status;
}

void
tracker::_update_tracks_by_new_vehicles(
  std::vector<triton::basic_detector::detection_box_t>& detected_areas)
{
  for (const auto& area : detected_areas) {
    auto appropriate_track =
      std::find_if(_tracks.begin(), _tracks.end(), [&area](track& t) {
        return area.first.contains(t.last_centroid());
      });

    if (appropriate_track == _tracks.end()) {
      auto features = _initialize_features(area.first);
      auto new_track = track{ features, area.second };
      _tracks.push_back(new_track);
    }
  }
}

uint64_t tracker::track::_leading_id = 0;

tracker::track::track(std::vector<cv::Point2f> features,
                      basic_detector::vehicle_type type)
  : _features(std::move(features))
  , _history{ _centroid() }
  , _id(++_leading_id)
  , _stamp(std::chrono::steady_clock::now())
  , _type(type)
{}

void
tracker::track::append(std::vector<cv::Point2f> features)
{
  _features = std::move(features);
  _stamp = std::chrono::steady_clock::now();
  _history.emplace_back(_centroid());
}

cv::Rect
tracker::track::bounding_rect() const
{
  auto horizontal_bounds =
    std::minmax_element(_features.begin(),
                        _features.end(),
                        [](const auto& a, const auto& b) { return a.x < b.x; });
  auto vertical_bounds =
    std::minmax_element(_features.begin(),
                        _features.end(),
                        [](const auto& a, const auto& b) { return a.y < b.y; });

  cv::Point2f top_left(horizontal_bounds.first->x, vertical_bounds.first->y);
  cv::Point2f bottom_right(horizontal_bounds.second->x,
                           vertical_bounds.second->y);

  return cv::Rect(top_left, bottom_right);
}

const std::vector<cv::Point2f>&
tracker::track::features() const
{
  return _features;
}

const std::vector<cv::Point2f>&
tracker::track::history() const
{
  return _history;
}

bool
tracker::track::is_expired() const
{
  return std::chrono::duration_cast<std::chrono::seconds>(
           std::chrono::steady_clock::now() - _stamp)
           .count() >= _dispose_timeout;
}

bool
tracker::track::is_invisible() const
{
  return _features.empty();
}

bool
tracker::track::is_laked() const
{
  return _features.size() < _minimum_features;
}

bool
tracker::track::is_outside(const cv::Rect& roi) const
{
  auto intersection = bounding_rect() & roi;
  return intersection.area() < bounding_rect().area();
}

bool
tracker::track::is_static() const
{
  if (_history.size() < _still_limit) {
    return false;
  }

  auto begin = _history.end() - _still_limit;
  auto end = _history.end() - 1;

  for (auto it = begin; it < end; it++) {
    auto dx = it->x - (it + 1)->x;
    auto dy = it->y - (it + 1)->y;
    auto distance = sqrt(dx * dx + dy * dy);
    if (distance > _distance_epsilon) {
      return false;
    }
  }

  return true;
}

const cv::Point2f&
tracker::track::last_centroid() const
{
  return _history.back();
}

void
tracker::track::refine_features(std::vector<uint8_t> status,
                                std::vector<float> errors)
{
  auto& centroid = last_centroid();
  for (auto i = 0; i < _features.size(); i++) {
    auto dx = _features[i].x - centroid.x;
    auto dy = _features[i].y - centroid.y;
    auto distance = sqrt(dx * dx + dy * dy);
    if (status[i] == 0 || errors[i] > _error_limit ||
        distance > _variation_limit) {
      _features[i].x = -1;
    }
  }
  _features.erase(std::remove_if(_features.begin(),
                                 _features.end(),
                                 [](auto& pt) { return pt.x == -1; }),
                  _features.end());
}

std::pair<tracker::track, tracker::track>
tracker::track::split()
{
  auto legatee = *this;
  legatee._id = ++_leading_id;
  return std::make_pair(*this, legatee);
}

std::chrono::steady_clock::time_point
tracker::track::stamp() const
{
  return _stamp;
}

basic_detector::vehicle_type
tracker::track::type() const
{
  return _type;
}

cv::Point2f
tracker::track::_centroid() const
{
  auto total_x = 0.f;
  auto total_y = 0.f;
  for (auto& point : _features) {
    total_x += point.x;
    total_y += point.y;
  }
  return cv::Point2f(total_x / _features.size(), total_y / _features.size());
}
