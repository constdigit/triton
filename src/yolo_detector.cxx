#include <fstream>

#include <opencv2/imgproc.hpp>

#include "yolo_detector.hxx"

using namespace triton;

yolo_detector::yolo_detector(std::string_view cfg,
                             std::string_view model,
                             std::string_view names,
                             float confidence_threshold)
  : _confidence_threshold(confidence_threshold)
  , _blob_size(320, 320)
{
  _yolo_network =
    cv::dnn::readNetFromDarknet(std::string(cfg), std::string(model));

  std::ifstream classes_names_file(names.data(), std::ios::in);
  if (!classes_names_file.is_open()) {
    throw std::runtime_error("Can't open file with classes names");
  }
  std::string class_name;
  while (std::getline(classes_names_file, class_name))
    _classes_names.push_back(class_name);
}

std::vector<basic_detector::detection_box_t>
yolo_detector::operator()(const cv::Mat& capture)
{
  auto input_blob = cv::dnn::blobFromImage(
    capture, 1 / 255.f, _blob_size, cv::Scalar(), true, false);
  _yolo_network.setInput(input_blob, _input_layer.data());
  auto detection_matrix = _yolo_network.forward(_output_layer.data());

  std::vector<basic_detector::detection_box_t> detected_objects;
  const auto probability_size = detection_matrix.cols - _probability_index;
  for (auto object_index = 0; object_index < detection_matrix.rows;
       object_index++) {
    auto* probability_array =
      &detection_matrix.at<float>(object_index, _probability_index);
    auto object_class = std::max_element(probability_array,
                                         probability_array + probability_size) -
                        probability_array;

    auto confidence = detection_matrix.at<float>(
      object_index, object_class + _probability_index);
    if (confidence < _confidence_threshold) {
      continue;
    }

    auto x_center = detection_matrix.at<float>(object_index, 0) * capture.cols;
    auto y_center = detection_matrix.at<float>(object_index, 1) * capture.rows;
    auto width = detection_matrix.at<float>(object_index, 2) * capture.cols;
    auto height = detection_matrix.at<float>(object_index, 3) * capture.rows;
    if (width < _minimal_width || height < _minimal_height) {
      continue;
    }

    cv::Point top_left(cvRound(x_center - width / 2),
                       cvRound(y_center - height / 2));
    cv::Point bottom_right(cvRound(x_center + width / 2),
                           cvRound(y_center + height / 2));
    cv::Rect object_bounding_rect(top_left, bottom_right);

    if (object_bounding_rect.x + object_bounding_rect.width >= capture.cols) {
      object_bounding_rect.width = capture.cols - object_bounding_rect.x;
    }
    if (object_bounding_rect.y + object_bounding_rect.height >= capture.rows) {
      object_bounding_rect.height = capture.rows - object_bounding_rect.y;
    }

    detected_objects.emplace_back(
      std::make_pair(object_bounding_rect, _vehicle_type(object_class)));
  }

  detected_objects.erase(
    std::remove_if(detected_objects.begin(),
                   detected_objects.end(),
                   [this](auto& area) { return _is_outside(area.first); }),
    detected_objects.end());

  return detected_objects;
}

std::string
yolo_detector::_class_name(size_t class_id) const
{
  return class_id < _classes_names.size()
           ? _classes_names[class_id]
           : std::string(cv::format("unknown(%d)", class_id));
}

basic_detector::vehicle_type
yolo_detector::_vehicle_type(std::string_view class_name) const
{
  if (class_name == "car") {
    return basic_detector::vehicle_type::car;
  }
  if (class_name == "bus") {
    return basic_detector::vehicle_type::bus;
  }
  if (class_name == "truck") {
    return basic_detector::vehicle_type::truck;
  }
  if (class_name == "bike") {
    return basic_detector::vehicle_type::bike;
  }
  return basic_detector::vehicle_type::unknown;
}

basic_detector::vehicle_type
yolo_detector::_vehicle_type(size_t class_id) const
{
  return _vehicle_type(_class_name(class_id));
}
